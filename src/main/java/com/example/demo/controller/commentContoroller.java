package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;

@Controller
public class CommentContoroller {
	@Autowired
	CommentService commentService;

	//  新規コメント画面(id=reportのid)
	@GetMapping("/comment/{id}")
	public ModelAndView commentContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用のentityを準備
		Comment comment = new Comment();
		comment.setReportId(id);
		// 準備したentityを保管
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/comment");
		return mav;
	}

	//  コメント返信処理(id=commentのid)
	@PostMapping("/reply/{id}")
	public ModelAndView replyContent(@ModelAttribute("formModel") Comment comment,
			@RequestParam("reportId") Integer reportId) {
		// reportIdをentityにセット
		comment.setReportId(reportId);
		// 返信と更新の判別
		boolean reply = true;
		// コメントをテーブルに格納
		commentService.saveComment(comment, reply);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//  コメント編集画面(id=commentのid)
	@GetMapping("/edit-comment/{id}")
	public ModelAndView editCommentContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集内容の取得
		Comment comment = commentService.editComment(id);
		// 編集内容を保管
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/edit-comment");
		return mav;
	}

	//	コメント編集処理(id=commentのid)
	@PutMapping("/update-comment/{id}")
	public ModelAndView updateCommentContent(@ModelAttribute("formModel") Comment comment, @PathVariable Integer id,
			@RequestParam("reportId") Integer reportId) {
		// idで更新対象を選択
		comment.setId(id);
		comment.setReportId(reportId);
		// 返信と更新の判別
		boolean reply = false;
		// 投稿をテーブルに格納
		commentService.saveComment(comment, reply);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント削除処理(id=commentのid)
	@DeleteMapping("/delete-comment/{id}")
	public ModelAndView deleteCommentContent(@PathVariable Integer id) {
		//投稿を削除
		commentService.deleteComment(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

}
