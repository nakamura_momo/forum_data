package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	// 投稿内容表示画面と日付絞り込み
	@GetMapping
	public ModelAndView top(@RequestParam(name = "startDate", required = false) String startDate,
			@RequestParam(name = "endDate", required = false) String endDate) {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport(startDate, endDate);
		// コメントを全件取得
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("startDate", startDate);
		mav.addObject("endDate", endDate);
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		return mav;

	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//削除
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		//投稿を削除
		reportService.deleteReport(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 編集画面の表示
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集内容の取得
		Report report = reportService.editReport(id);
		// 編集内容を保管
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@ModelAttribute("formModel") Report report, @PathVariable Integer id) {
		// idで更新対象を選択
		report.setId(id);
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

}
