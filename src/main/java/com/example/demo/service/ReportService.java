package com.example.demo.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport(String startDate, String endDate) {
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date start = null, end = null;
		try {
			if (StringUtils.isEmpty(startDate)) {
				//投稿の絞り込みのためのデフォルト日時取得
				startDate = "2020-01-01 00:00:00";
				start = sdFormat.parse(startDate);
			} else {
				start = sdFormat.parse(startDate + " 00:00:00");
			}

			if (StringUtils.isEmpty(endDate)) {
				//投稿の絞り込みのためのデフォルト日時取得
				end = new Date();
			} else {
				end = sdFormat.parse(endDate + " 23:59:59");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		// 日付による絞り込み、updateの昇順で並び替え
		return reportRepository.findByCreatedDateBetweenOrderByUpdatedDateDesc(start, end);

	}

	// レコード追加と編集
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// 編集画面の表示
	public Report editReport(Integer id) {
		return reportRepository.findById(id).orElse(null);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}
}