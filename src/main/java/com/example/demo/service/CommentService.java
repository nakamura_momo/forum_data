package com.example.demo.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.ReportRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;
	@Autowired
	ReportRepository reportRepository;

	// コメント全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
	}

	// コメント追加と編集
	public void saveComment(Comment comment, boolean reply) {
		// コメント追加の場合、updated_dateを更新
		if (reply == true) {
			Report report = reportRepository.findById(comment.getReportId()).orElse(null);
			report.setUpdatedDate(new Date());
			reportRepository.save(report);
		}
		commentRepository.save(comment);
	}

	// コメント編集画面の表示
	public Comment editComment(Integer id) {
		return commentRepository.findById(id).orElse(null);
	}

	// コメント削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

}